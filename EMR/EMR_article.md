# An Annotated Corpus of Corelli’s Trio Sonatas

AUTEURS :
- JÉRÉMIE ROUX : LIRMM, Univ Montpellier, CNRS, Montpellier, France
- MATHIEU GIRAUD : CRIStAL, UMR 9189 CNRS, Université de Lille, France
- FLORENCE LEVÉ : MIS, Université de Picardie Jules Verne, France - CRIStAL, UMR 9189 CNRS, Université de Lille, France

INSTRUCTIONS : 
- https://emusicology.org/index.php/EMR/authorguidelines
- voir [TEMPLATE](EMR_article.odt) (.odt)

Derniers DATA REPORTS : 
- https://emusicology.org/index.php/EMR/article/view/8903
- https://emusicology.org/index.php/EMR/article/view/8927
- https://emusicology.org/index.php/EMR/article/view/9281
- https://emusicology.org/index.php/EMR/article/view/8531
- https://emusicology.org/index.php/EMR/article/view/8511
- https://emusicology.org/index.php/EMR/issue/view/278 (plusieurs)

ABSTRACT: 

KEYWORDS: corpora, Corelli, sonata, baroque

## Intro musico



## Enjeu et choix de modélisation

## Le corpus et quelques stats 