# Stage de M2 - Analyse et génération de contrepoint baroque : étude de cas des sonates corelliennes

- **Etudiant :** [Jérémie ROUX](https://jeremieroux.fr/) (M2 IASD - [Université de Montpellier](https://www.umontpellier.fr/))

- **Laboratoire :** [Algomus](http://www.algomus.fr/) - [CRIStAL](https://www.cristal.univ-lille.fr/) (UMR CNRS 9189) - Cité Scientifique de Villeneuve d’Ascq (59)

- **Encadrement :**
  
  - [Mathieu GIRAUD](http://cnrs.magiraud.org/) (Directeur de recherche | [CNRS](https://www.cnrs.fr/fr) | [Université de Lille](https://www.univ-lille.fr/))
  
  - [Florence LEVÉ](https://home.mis.u-picardie.fr/~leve/) (Maître de conférences | [MIS](https://www.mis.u-picardie.fr/) | [Université de Picardie](https://www.u-picardie.fr/))

- **Période :** 01/02/2023 - 30/06/2023

## Travail effectué

### Recherche

- [Étude bibliographique préalable](rapport/etudeBiblioCorelli.pdf)

- [Étude bibliographique (Zotero)](https://www.zotero.org/groups/4949625/stage_de_m2/library)

- Annotation manuelle des opus 1-3-4 des sonates de Corelli

- [Rapport de présentation du stage (date limite le 15/03/2023)](rapport/rapport.pdf)

- Rapport final (date limite le 12/06/2023)

### Programmation

- Récupération du [jeu de données DCMLab](https://github.com/DCMLab/corelli) ([+ d'infos](https://dcmlab.github.io/corelli))
- [Statistiques sur les intervalles mélodiques dans les opus 1-3-4 des sonates de Corelli](prog/intervallesMelodiques.py)

### Calcul des statistiques de l'article (Roux, 2024)

- [Général](partitions_annotations_synchro/stats.py) : `python partitions_annotations_synchro/stats.py`
- [Calculer les ratios de position de chaque annotation au sein des phrases musicales -> .dez.phrase](partitions_annotations_synchro/phrase.py) : `python partitions_annotations_synchro/phrase.py`
- [Densité](partitions_annotations_synchro/stats_densite.py) : `python partitions_annotations_synchro/stats_densite.py`
- [Octave](partitions_annotations_synchro/stats_octave.py) : `python partitions_annotations_synchro/stats_octave.py`
- [Structure](partitions_annotations_synchro/stats_structure.py) : `python partitions_annotations_synchro/stats_structure.py`
- [Texture](partitions_annotations_synchro/stats_texture.py) : `python partitions_annotations_synchro/stats_texture.py`


