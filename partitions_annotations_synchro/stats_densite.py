import os
import json
import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib as mpl

densite = {
    "1" : ["1"],
    "1/2" : ["1","2"],
    "1/4" : ["1","4"],
    "1/8" : ["1","2","4","8"],
    "16" : ["16"],
    "2" : ["2"],
    "2/16" : ["2","4","8","16"],
    "2/4" : ["2","4"],
    "2/8" : ["2","4","8"],
    "4" : ["4"],
    "4/16" : ["4","8","16"],
    "4/8" : ["4","8"],
    "8" : ["8"],
    "8/16" : ["8","16"]
}

pas = 0.02
maximum = 2.5
max_density=True
t = {}
for e in ["16","8","4","2","1"]:
    t[e]=[0 for i in range(int(maximum/pas))]

directory = 'partitions_annotations_synchro/annotation_dezrann'
for filename in os.listdir(directory):
    if "phrase" in filename:
        #print(filename)
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)

        for e in data["labels"]:
            if "H" in e["tag"]:
                e["tag"]=e["tag"][0]
            print(e)
            if e["type"] == "Rhythmic Density" and "-" not in e["position-ratio-start"]:
                idDebut = int(float(e["position-ratio-start"])/pas)
                idFin = int(float(e["position-ratio-end"])/pas)
                #print(idDebut,idFin)
                for i in range(idDebut,idFin+1):
                    if len(t[densite[e["tag"]][-1]])>i:
                        if max_density:
                            t[densite[e["tag"]][-1]][i]+=1
                        else:
                            for d in densite[e["tag"]]:
                                t[d][i]+=1

print(t)

fig, ax = plt.subplots()
bottom = np.zeros(int(maximum/pas))

whole = np.array(t["1"])
half = np.array(t["2"])
quarter = np.array(t["4"])
eighth = np.array(t["8"])
sixteenth = np.array(t["16"])

X = []
i = 0
while(i<maximum):
    X.append(i+pas/2)
    i+=pas

plt.bar(X, whole, color = 'blue', width=0.02, label="Whole note")
plt.bar(X, half, color = 'orange', bottom = whole, width=0.02, label="Half note")
plt.bar(X, quarter, color = 'green', bottom = whole+half, width=0.02, label="Quarter note")
plt.bar(X, eighth, color = 'red', bottom = whole+half+quarter, width=0.02, label="Eighth note")
plt.bar(X, sixteenth, color = 'purple', bottom = whole+half+quarter+eighth, width=0.02, label="Sixteenth note")

plt.ylabel('Number of such labels', fontsize=15)
plt.xlabel('Localization in the musical phrase (0.0 = begin, 1.0 = end)', fontsize=15)
ax.set_xlim([0, 1.5])
#ax.set_ylim([0, 250])
ax.tick_params(axis='both', which='major', labelsize=14)

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[::-1], labels[::-1], title='Rhythm figure', title_fontsize=15, loc='upper right', prop={'size': 14})

plt.show()