## Squelettes typiques de mouvement de sonate en trio

- I - V - I - VI/rel - I

- Ralentissement de la densité majoritaire à l'approche des cadences

- Parties contrastantes
  
  - Type 1
    
    - Homorythmies
    
    - Saut d'octaves à l'approche des cadences
    
    - Retards 4-3 et 7-6
  
  - Type 2 : Imitations (entre 1 et 3 reproductions)
    
    - Type 2a : Beaucoup de retards 9-8, 7-6 et 4-3 (le motif est conçu comme tel)
    
    - Type 2b : quasiment aucun retard

Enchaînement 1

1. A - I - Type 1

2. A - V - Type 1

3. B - I vers VI/rel - Type 2

4. C - I - Type 2

Enchaînement 2

1. A - I - Type 1

2. B - I vers V - Type 2

3. C - I vers VI/rel - Type 1

4. D - I - Type 2

5. D - I - Type 2
