
import glob
import json
from rich import print

LAST = 1000

def ratios(x, x_end, structures, cadences, i_shift=0):
    ss = structures + [LAST]

    # Locate current and next structure
    i_current = None
    i_next = None

    for i in range(len(ss)):
        if ss[i] <= x:
            i_current = i
        if ss[i] > x:
            i_next = i
            break

    struct = ss[i_current]
    struct_next = ss[i_next]

    # Locate last cadence before next structure
    cadence = max([cad for cad in cadences if cad < struct_next])
    if cadence < struct:
        print(f'[red]No cadence for struct {struct}')
    ratio = (x-struct) / (cadence-struct)

    overflow = (x_end > struct_next)
    if overflow:
        x_end = struct_next
        overflow = struct_next
    ratio_end = (x_end-struct) / (cadence-struct)
    # print(x, 'struct', struct, f'{ratio:.3f}', f'{ratio_end:.3f}', 'cad', cadence, struct_next)

    return ratio, ratio_end, overflow

def phrase(f):
    print(f'[purple]<== {f}')
    d = json.load(open(f))
    
    cadences = []
    structures = []
    for l in d['labels']:
        if not 'start' in l:
            l['start'] = 0
        if not 'duration' in l:
            l['duration'] = 0
        if l['type'] == 'Structure' and 'Algomus' in l['layers']    :
            structures.append(l['start'])
        if l['type'] == 'Cadence':
            cadences.append(l['start'])
            
    # Add k

    print(f'Struct: {structures}')
    print(f'Cad: {cadences}')
   
    if len(structures) < len(cadences):
        print(f'[yellow] {len(cadences)-len(structures)} cadences without structure')

    new_labels = []

    for l in d['labels']:

        shift = 0
        ongoing = True
        qstart = l['start']

        while ongoing:

            start, end, qstart = ratios(l['start'],
                                        l['start'] + l['duration'],
                                        structures, cadences)

            l['position-ratio-start'] = f"{start:.3f}"
            l['position-ratio-end'] = f"{end:.3f}"

            ongoing = (qstart > 0)

            # save current label
            ll = l.copy()
            ll['duration'] = (qstart - l['start']) if qstart else l['duration']
            new_labels += [ ll ]

            if "ensit" in ll['type']:
              print(f"  [red]{ll['type']:20s}[/]",
                  f"[purple]{ll['start']:5.1f} → {ll['start'] + ll['duration']:5.1f}[/]",
                  f"[yellow]+{shift}[/]" if shift else '  ',
                  f"{start:.3f} → {end:.3f}   {ll['tag']}")

            # overflow label: shift start
            l['duration'] -= qstart - l['start']
            l['start'] = qstart

            shift += 1

    d['labels'] = new_labels
    ff = f + '.phrase'
    print(f'[purple]==> {ff}')
    json.dump(d, open(ff, 'w'), indent=2)
    print()




if __name__ == '__main__':
    for f in glob.glob('partitions_annotations_synchro/annotation_dezrann/*dez'):
        phrase(f)

