import os
import json
import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib as mpl
from matplotlib.patches import Patch
import matplotlib.ticker as mtick

texSimpl = {
    'h' : 0,
    'p' : 0,
    'i' : 0,
    'a' : 0
}
total = 0

H3 = '#77aaff'
P3 = '#7788dd'
H2 = '#aaaaff'
P2 = '#aa88dd'
I3 = '#ddaa44'
I2 = '#ffcc88'

LABELS = [
    ('h123', H3),
    ('p123', P3),

    ('h12', H2),
    ('p12', P2),
    ('h13', H2),
    ('p13', P2),
    ('h23', H2),
    ('p23', P2),

#   ('none', 'lightgray'),

    ('i[12][3]', I3),
    ('i[13][2]', I3),
    ('i[1][23]', I3),
    ('i[1][2]', I2),
    ('i[1][2][3]', I3),
    ('i[1][3]', I2),
    ('i[23][1]', I3),
    ('i[2][1]', I2),
    ('i[2][3]', I2),
    ('i[3][12]', I3),
    ('i[3][1]', I2),
    ('i[3][2]', I2),

    ('a[13]', I2),
    ('a[1][2]', I2),
    ('a[1][23]', I3),
    ('a[2][1]', I2),
    ('a[3][1]', I2),
    ('a[3]', I2)
]
NONE = 8

tex = { label:0 for (label, _) in LABELS }

def format_im(label):
    # i[23][1] ==> i23/1
    return label.replace('][', '/').replace('[','').replace(']','')


dict_num = {'1':'a','2':'b','3':'c','4':'d'}
directory = 'partitions_annotations_synchro/annotation_dezrann'
for filename in os.listdir(directory):
    if "phrase" not in filename :
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)

        print(filename)
        for e in data["labels"]:
            if e["type"] == "Melodic Movement":
                if e["tag"] in list(tex.keys()):
                    if "duration" not in e :
                        tex[e["tag"]]+=4
                    else :
                        tex[e["tag"]]+=e["duration"]
                else :
                    tex[e["tag"]]=e["duration"]
                
                if e["tag"][0] in list(texSimpl.keys()):
                    if "duration" not in e :
                        texSimpl[e["tag"][0]]+=4
                    else :
                        texSimpl[e["tag"][0]]+=e["duration"]
                else :
                    texSimpl[e["tag"][0]]=e["duration"]


        refDCML = filename.split('-')
        refDCML[0] = "0"+refDCML[0][2:]
        refDCML[1] = refDCML[1] if len(refDCML[1])==2 else "0"+refDCML[1]
        refDCML[2] = dict_num[refDCML[2][:-4]]
        refDCML = "op"+refDCML[0]+"n"+refDCML[1]+refDCML[2]+".harmonies.tsv"
        df = pd.read_csv("partitions_annotations_synchro/harmonies/"+refDCML,sep='\t').fillna("")
        total += float(df.iloc[-1]["quarterbeats"])+float(df.iloc[-1]["duration_qb"])


def ajoutL(id,d):
    res = []
    somme = 0
    for e in d:
        if id in e:
            res.append(d[e]/total)
            somme+=d[e]
        else:
            res.append(0)
    res.insert(NONE,(total-somme)/total)
    return res

print("TOTAL :",total)
print("Homorythmie :",texSimpl["h"],"soit",texSimpl["h"]/total)
print("Parallelisme :",texSimpl["p"],"soit",texSimpl["p"]/total)
print("Imitation :",texSimpl["i"],"soit",texSimpl["i"]/total)
print("Antiphonie :",texSimpl["a"],"soit",texSimpl["a"]/total)

print(tex)
category_names = list(map(format_im, tex.keys()))
category_names.insert(8,"none")
results = {
    'Violin I (1)': ajoutL("1",tex),
    'Violin II (2)': ajoutL("2",tex),
    'Continuo (3)': ajoutL("3",tex)
}

print(results)


def survey(results, category_names):
    labels = list(results.keys())
    data = np.array(list(results.values()))
    data_cum = data.cumsum(axis=1)
    colors = [ color for (_, color) in LABELS ]
    colors.insert(NONE,"lightgrey")
    fig, ax = plt.subplots(figsize=(18, 5))
    ax.invert_yaxis()
    ax.set_xlim(0, np.sum(data, axis=1).max())
    for i, (colname, color) in enumerate(zip(category_names, colors)):
        widths = data[:, i]
        starts = data_cum[:, i] - widths
        rects = ax.barh(labels, widths, left=starts, height=0.6,
                        label=colname, color=color)

    id=0
    for c in ax.containers:
        labels = [category_names[id] if (w := v.get_width()) > 0.03 and category_names[id] != 'none' else '' for v in c ]
        ax.bar_label(c, labels=labels, label_type='center', size=16)
        id+=1

    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.set_xlabel('Duration', fontsize=15)
    ax.xaxis.set_major_formatter(mtick.PercentFormatter(1.0))

    legend = [
        Patch(facecolor=H3, label='Homorhythmy (three voices)'),
        Patch(facecolor=H2, label='Homorhythmy (two voices)'),
        Patch(facecolor=P3, label='Parallelism (three voices)'),
        Patch(facecolor=P2, label='Parallelism (two voices)'), 
        Patch(facecolor=I3, label='Imitation and antiphony (three voices)'),
        Patch(facecolor=I2, label='Imitation and antiphony (two voices)')
    ]

    ax.legend(handles=legend,
              ncols=3, frameon=False,
              bbox_to_anchor=(0.5, 1.3),
              loc='upper center',
              prop={'size': 15})

    return fig, ax


survey(results, category_names)
plt.tight_layout()
plt.show()

