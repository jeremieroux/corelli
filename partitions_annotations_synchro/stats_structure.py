import os
import json
import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib as mpl

result = {}
result_type = {}
liste = []
dict_num = {'1':'I','2':'II','3':'III','4':'IV'}
directory = 'partitions_annotations_synchro/annotation_dezrann'
for filename in os.listdir(directory):
    if "phrase" not in filename :
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)
        f = filename.split('-')
        f = "Op."+f[0][2:]+" No."+f[1]+" "+dict_num[f[2][:-4]]
        print(f)

        for e in data["labels"]:
            if e["type"] == "Structure" and e["layers"] == ["Algomus"]:
                if f not in result.keys():
                    result[f]=0
                if e["tag"][1:] not in result_type.keys():
                    result_type[e["tag"][1:]]=0
                result[f]+=1
                result_type[e["tag"][1:]]+=1
        liste.append(result[f])

result["AVERAGE"]=sum(liste)/len(liste)
result = {k: v for k, v in sorted(result.items(), key=lambda item: item[1])}
print(json.dumps(result, indent=2))
print(json.dumps(result_type, indent=2))

y=list(result.keys())
x=list(result.values())
barlist=plt.barh(y, x)
barlist[24].set_color('r')
plt.ylabel("Movement reference", fontsize=15)
plt.xlabel("Number of structures", fontsize=15)
plt.tick_params(axis='x', which='major', labelsize=13)
plt.xticks(np.arange(0, 12, 1))
plt.show()