import os
import json
import csv
import pandas as pd

dict_num = {'1':'a','2':'b','3':'c','4':'d'}
result = {}
result_DCML = {"ph" : {}, "cad" : {}, "mod" : {}, "deg" : {}, "quality" : {}}
directory = 'partitions_annotations_synchro/annotation_dezrann'
for filename in os.listdir(directory):
    if "phrase" not in filename :
        refDCML = filename.split('-')
        refDCML[0] = "0"+refDCML[0][2:]
        refDCML[1] = refDCML[1] if len(refDCML[1])==2 else "0"+refDCML[1]
        refDCML[2] = dict_num[refDCML[2][:-4]]
        refDCML = "op"+refDCML[0]+"n"+refDCML[1]+refDCML[2]+".harmonies.tsv"
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)

        for e in data["labels"]:
            if e["type"] not in result.keys():
                result[e["type"]]={}
            if e["tag"] in result[e["type"]].keys():
                result[e["type"]][e["tag"]]+=1
            else:
                result[e["type"]][e["tag"]]=1

        df = pd.read_csv("partitions_annotations_synchro/harmonies/"+refDCML,sep='\t').fillna("")
        for index, row in df.iterrows():
            if row["numeral"] != "":
                if row["numeral"] not in result_DCML["deg"].keys():
                    result_DCML["deg"][row["numeral"]]=1
                else:
                    result_DCML["deg"][row["numeral"]]+=1
            if str(row["figbass"]) != "":
                if str(row["figbass"]) not in result_DCML["quality"].keys():
                    result_DCML["quality"][str(row["figbass"])]=1
                else:
                    result_DCML["quality"][str(row["figbass"])]+=1
            if row["phraseend"] != "":
                if row["phraseend"] not in result_DCML["ph"].keys():
                    result_DCML["ph"][row["phraseend"]]=1
                else:
                    result_DCML["ph"][row["phraseend"]]+=1
            if row["localkey"] != "":
                if row["localkey"] not in result_DCML["mod"].keys():
                    result_DCML["mod"][row["localkey"]]=1
                else:
                    result_DCML["mod"][row["localkey"]]+=1
            if row["cadence"] != "":
                if row["cadence"] not in result_DCML["cad"].keys():
                    result_DCML["cad"][row["cadence"]]=1
                else:
                    result_DCML["cad"][row["cadence"]]+=1

for e in result:
    d = dict(sorted(result[e].items()))
    print(e)
    for k in d:
        print("\""+k+"\" ("+str(d[k])+")", end=", ")
    print()

print()

for e in result_DCML:
    d = dict(sorted(result_DCML[e].items()))
    for k in d:
        print("\""+k+"\" ("+str(d[k])+")", end=", ")
    print()
    
"""eeeeee
cumul = 0
df = pd.read_csv("partitions_annotations_synchro/harmonies/"+refDCML,sep='\t').fillna("")
lastmod = ""
mod = []
cad = []
ph = []
for index, row in df.iterrows():
    ligne = {}
    ligne["type"] = "Harmony"
    ligne["tag"] = row['numeral']
    ligne["staff"] = "4"
    ligne["duration"] = float(row['duration_qb'])
    if cumul!=0:
        ligne["start"] = cumul
    ligne["layers"] = ["DCML"]
    
    if result["labels"][-1]["tag"]==ligne["tag"]:
        result["labels"][-1]["duration"]+=ligne["duration"]
    elif ligne["tag"] != "":
        result["labels"].append(ligne)
    
    if lastmod==row["localkey"]:
        mod[-1]["duration"]+=ligne["duration"]
    else:
        modulation = {}
        modulation["type"] = "Modulation"
        modulation["tag"] = row["localkey"]
        lastmod=row["localkey"]
        modulation["duration"] = float(row['duration_qb'])
        if cumul!=0:
            modulation["start"] = cumul
        modulation["layers"] = ["DCML"]
        mod.append(modulation)
    
    if row["cadence"]!="":
        cadence = {}
        cadence["type"] = "Cadence"
        cadence["tag"] = row["cadence"]
        cadence["duration"] = 0
        if cumul!=0:
            cadence["start"] = cumul
        cadence["layers"] = ["DCML"]
        cad.append(cadence)

    if row["phraseend"]!="":
        phrase = {}
        phrase["type"] = "Structure"
        phrase["tag"] = row["phraseend"]
        phrase["duration"] = 0
        if cumul!=0:
            phrase["start"] = cumul
        phrase["layers"] = ["DCML"]
        ph.append(phrase)

    cumul+=float(row['duration_qb']) 
"""