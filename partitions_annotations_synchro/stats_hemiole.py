import os
import json
import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib as mpl
import math

result = {}
result_type = {}
liste = []
directory = 'partitions_annotations_synchro/annotation_dezrann'

'''for filename in os.listdir(directory):
    if "phrase" in filename :
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)
        for e in data["labels"]:
            if e["type"] == "Rhythmic Density" and "H" in e["tag"]:
                print(round(float(e["position-ratio-start"]),2),round(float(e["position-ratio-end"]),2))'''

val_mvt = [0 for x in range(20)]
for filename in os.listdir(directory):
    if "phrase" not in filename :
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)
        duree_mvt = data["labels"][-1]["start"]
        for e in data["labels"]:
            if e["type"] == "Rhythmic Density" and "H" in e["tag"]:
                debut = round(e["start"]/duree_mvt,2)
                fin = round((e["start"]+e["duration"])/duree_mvt,2)
                #print(debut,math.floor(debut/0.05),fin,math.ceil(fin/0.05))
                for i in range(math.floor(debut/0.05),math.ceil(fin/0.05)+1):
                    val_mvt[i-1]+=1

data = {}
for i,v in enumerate(val_mvt):
    data[str(round(i*0.05,2))]=v
data["1.0"]=0
courses = list(data.keys())
values = list(data.values())
fig = plt.figure(figsize = (20, 5))
plt.bar(courses, values, color ='blue', width = 0.7)
plt.xlabel("Localization in the movement (0.0 = begin, 1.0 = end)", fontsize=15)
plt.ylabel("Number of hemiolas", fontsize=15)
plt.tick_params(axis='both', which='major', labelsize=15)
plt.xticks(np.arange(-0.5, 20, 2))
plt.show()
