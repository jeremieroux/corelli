Choix du format pour les tonalités :

- En toutes lettres : la mineur

- Abrégé : La min / Do Maj

- Avec notation anglo-saxones : A min / C Maj

- Abréviation 





Tonalité principale : A min (La mineur)

Tonalité d'emprunt/modulation : C Maj (Do Majeur)

- Rapport avec la tonalité principale ou la tonalité précédente (relatif majeur, do Majeur est le degré III de la mineur)

- À partir de combien de mesures considère-t-on que l'emprunt est effectué ? (à différencier avec des successions de dominantes secondaires par exemple)

- Rapport avec la tonalité principale ou la tonalité précédente (relatif majeur, do Majeur est le degré III de la mineur)

- À partir de combien de mesures considère-t-on que l'emprunt est effectué ? (à différencier avec des successions de dominantes secondaires par exemple)

Cycle des degrés en chiffres romains : 

- I (tonique)

- II ou II nap (prédominantes)

- III (souvent absent car le plus souvent V/V)

- IV (prédominante)

- V (dominante)

- VI (prédominante)

- VII (non-usité dans ce style)
