#!/bin/bash

FILES="./corelli-main/MS3/*.mscx"

mkdir -p "conversion"

for f in $FILES
do
	nom=${f%.mscx}
	nom=${nom##*/}
	nom2=${nom#*op0}
	opus=${nom2::1}

	mvt=${nom2:0-1}
	dic=${mvt/a/1}
	dic=${dic/b/2}
	dic=${dic/c/3}
	dic=${dic/d/4}
	dic=${dic/e/5}
	dic=${dic/f/6}
	dic=${dic/g/7}

	nom2=${nom2%?}
	num=${nom2:2}
	if [[ $num = 0* ]]
	then
		num=${num:1}
	fi

	echo "$nom.mscx -> op$opus.$num.$dic.musicxml"

	/Applications/MuseScore\ 4.app/Contents/MacOS/mscore $f -o "./conversion/op$opus.$num.$dic.musicxml"
done