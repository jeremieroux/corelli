import os
import json
import csv
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import matplotlib as mpl

dict_num = {'1':'a','2':'b','3':'c','4':'d'}
detail=True
d = {}

nb_octave = {'1' : {'o↘' : 0, 'o↗' : 0}, '2' : {'o↘' : 0, 'o↗' : 0}, '3' : {'o↘' : 0, 'o↗' : 0}}

directory = 'partitions_annotations_synchro/annotation_dezrann'
for filename in os.listdir(directory):
    if "phrase" not in filename :
        refDCML = filename.split('-')
        refDCML[0] = "0"+refDCML[0][2:]
        refDCML[1] = refDCML[1] if len(refDCML[1])==2 else "0"+refDCML[1]
        refDCML[2] = dict_num[refDCML[2][:-4]]
        refDCML = "op"+refDCML[0]+"n"+refDCML[1]+refDCML[2]+".harmonies.tsv"
        f = os.path.join(directory, filename)
        file = open(f)
        data = json.load(file)

        d[filename] = {"octave" : [], "cadence" : []}

        for e in data["labels"]:
            if e["type"] == "Pattern" and "o" in e["tag"]:
                nb_octave[e['staff']][e['tag']]+=1
                debut = 0 if "start" not in e else int(e["start"])
                duree = 1 if "duration" not in e else int(e["duration"])
                if detail : 
                    d[filename]["octave"].append([e["tag"]+str(e["staff"]),debut,debut+duree])
                else : 
                    d[filename]["octave"].append([e["tag"],debut,debut+duree])

        df = pd.read_csv("partitions_annotations_synchro/harmonies/"+refDCML,sep='\t').fillna("")
        cumul=0
        for index, row in df.iterrows():
            if row["cadence"] != "":
                if "/" in str(row["quarterbeats"]):
                    frac = str(row["quarterbeats"]).split("/")
                    d[filename]["cadence"].append([row["cadence"],cumul,int(frac[0])/int(frac[1])])
                    cumul = int(frac[0])/int(frac[1])
                else:
                    d[filename]["cadence"].append([row["cadence"],cumul,int(row["quarterbeats"])])
                    cumul = int(row["quarterbeats"])

#print(json.dumps(d, indent=2))
print(nb_octave)

t = {}
pas = 20
max = False

if detail :
    for e in ["o↗1", "o↘1","o↗2", "o↘2","o↗3", "o↘3"]:
        t[e]=[0 for i in range(pas)]
else :
    for e in ["o↗","o↘"]:
        t[e]=[0 for i in range(pas)]

for mvt in d :
    #print(mvt)
    id_octave = 0
    for cad in d[mvt]["cadence"]:
        id_section = 0
        section = [cad[1]+(i*(cad[2]-cad[1])/pas) for i in range(1,pas+1)]
        while id_section<pas and id_octave<len(d[mvt]["octave"]):
            #print(cad, section, d[mvt]["octave"][id_octave])
            if d[mvt]["octave"][id_octave][2]>=section[id_section]:
                #print(d[mvt]["octave"][id_octave][0],section[id_section])
                e = d[mvt]["octave"][id_octave][0]
                #print(e,id_section)
                t[e][id_section]+=1
                id_section+=1  
            else:
                id_octave+=1

note = []
if detail :
    note = ["o↗ (Violin I)", "o↘ (Violin I)","o↗ (Violin II)", "o↘ (Violin II)","o↗ (Continuo)", "o↘ (Continuo)"]
else :
    note = ["o↗", "o↘"]

tableau = []
if detail :
    tableau = np.array([t["o↗1"],t["o↘1"],t["o↗2"],t["o↘2"],t["o↗3"],t["o↘3"]])
else :
    tableau = np.array([t["o↗"],t["o↘"]])

fig, ax = plt.subplots()
im = ax.imshow(tableau, cmap="autumn_r")
ax.set_yticks(np.arange(len(note)), labels=note)
#print([(1/pas)*i for i in range(pas)])
#print(np.arange(pas))
ax.set_xticks([float(n)-0.5 for n in np.arange(pas+1)],labels=[str((1/pas)*i)[:4] for i in range(pas+1)])
#ax.get_xaxis().set_visible(False)
ax.tick_params(axis='both', which='major', labelsize=15)
plt.xlabel('Percentage of progress in the area between two cadences', fontsize=15)
plt.setp(ax.get_xticklabels(), ha="center", rotation_mode="anchor")
for i in range(len(note)):
    for j in range(pas):
        text = ax.text(j, i, tableau[i, j], ha="center", va="center", color="black", size=14)
fig.tight_layout()
plt.show()


species = (
    "0-0.05",
    "0.05-0.1",
    "0.1-0.15",
    "0.15-0.2",
    "0.2-0.25",
    "0.25-0.3",
    "0.3-0.35",
    "0.35-0.4",
    "0.4-0.45",
    "0.45-0.5",
    "0.5-0.55",
    "0.55-0.6",
    "0.6-0.65",
    "0.65-0.7",
    "0.7-0.75",
    "0.75-0.8",
    "0.8-0.85",
    "0.85-0.9",
    "0.9-0.95",
    "0.95-1"
)
weight_counts = {
    "o↗ (Continuo)": np.array(t["o↗3"]),
    "o↘ (Continuo)": np.array(t["o↘3"]),
    "o↗ (Violin I)": np.array(t["o↗2"]),
    "o↘ (Violin I)": np.array(t["o↘2"]),
    "o↗ (Violin II)": np.array(t["o↗1"]),
    "o↘ (Violin II)": np.array(t["o↘1"])
}
width = 0.5

fig, ax = plt.subplots()
bottom = np.zeros(20)
colors = ['darkred', 'red', 'darkblue', 'blue', 'darkgreen', 'green']

i=0
for boolean, weight_count in weight_counts.items():
    p = ax.bar(species, weight_count, width, label=boolean, bottom=bottom, color = colors[i])
    bottom += weight_count
    i+=1


plt.ylabel('Number of such labels', fontsize=15)
plt.xlabel('Localization in the musical phrase (0.0 = begin, 1.0 = end)', fontsize=15)
ax.set_xlim([0, 1])
ax.set_ylim([0, 250])
ax.tick_params(axis='both', which='major', labelsize=15)
ax.set_xticks([float(n)-0.5 for n in np.arange(pas+1)],labels=[str((1/pas)*i)[:4] for i in range(pas+1)])
ax.legend(ncols=3, loc='upper right', title='Octave jump direction and voice', title_fontsize=15, prop={'size': 14})

plt.show()
