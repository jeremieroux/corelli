\chapter{Transformation des données musicales en annotations structurées}

Les annotations correspondent à la transformation de l'information musicale en texte pouvant alors être interprétée par un programme informatique. Pour expliquer la manière dont nous allons annoter les mouvements de sonate, prenons la sonate no. 1 de l'opus 1, 1er mouvement (Grave). Nous procédons tout d'abord à une analyse sur partition (voir \textsc{Figure \ref{fig:111}}). On constate la présence d'éléments musicaux récurrents mis en évidence par des couleurs.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\columnwidth]{figures/111.png}
    \caption{Analyse préalable sur papier (Opus 1, Sonate no. 1, I - Grave)}
    \label{fig:111}
\end{figure}

Pour simplifier, voici les notations que nous donnons à ces annotations avec entre parenthèse la taille de chaque vocabulaire :
\begin{itemize}\setlength\itemsep{-0.5em}	
	\item $A_m$ : Annotations sur mesure
	\begin{itemize}\setlength\itemsep{-0.2em}	
		\item $S$ : Métastructures
		\begin{itemize}\setlength\itemsep{-0.2em}
			\item $S_n$ : Nom des métastructures (6)
			\item $S_t$ : Textures générales des métastructures (7)
		\end{itemize}
		\item $V$ : Mouvements (3)
		\item $T$ : Tonalités des zones modulantes (6)
		\item $C$ : Cadences (5)
		\item $D$ : Densités rythmiques majoritaires (5)
		\item $M$ : Motifs
		\begin{itemize}\setlength\itemsep{-0.2em}	
			\item $M_i$ : Motifs impliqués dans des imitations (8)
			\item $M_a$ : Autres motifs récurrents (4)
			\item $M_o$ : Sauts d'octave (2)
		\end{itemize}
	\end{itemize}
	\item $A_d$ : Annotations de DCMLab
	\begin{itemize}\setlength\itemsep{-0.2em}	
		\item $H$ : Fonction harmonique de chaque accord
		\begin{itemize}\setlength\itemsep{-0.2em}	
			\item $H_d$ : Degré des accords (14)
			\item $H_c$ : Chiffrage des accords
		\end{itemize}
	\end{itemize}
	\item $A_p$ : Annotations liées au contenu de la partition
	\begin{itemize}\setlength\itemsep{-0.2em}	
	\item $N$ : Données concernant les notes
		\begin{itemize}\setlength\itemsep{-0.2em}	
			\item $N_v$ : Voix (3)
			\item $N_h$ : Hauteur des notes
			\item $N_d$ : Durée des notes
		\end{itemize}
	\end{itemize}
\end{itemize}

\section{Qu'appelons-nous une annotation ?}

\begin{algorithm}[h!]
\small
\Variables{$A$ : \textit{Annotation}, $M$ : liste des annotations du mouvement, $D$ : dictionnaire}
$D \gets dictionnaireVide()$\;
\PourCh{fichier $f$ du dossier d'analyse $d$}{
$nomF \gets nomSansSuffixe(f)$\tcp*{Récupération du nom du fichier}
$M \gets listeVide()$\;
\PourCh{ligne $l$ de $f$}{
$l \gets JSON(l)$\;
$A \gets (l.type$, $l.valeur$, arrondir($l.debut,g$), arrondir($l.debut+l.durée,g$))\;
$M.ajouter(A) $\tcp*{Ajouter l'annotation dans la liste}
}
$D[nomF] \gets M$\tcp*{Ajouter une nouvelle entrée dans le dictionnaire}
}
\Kwrenv $D$\;
\caption{StockerAnnotations($d$ : dossier contenant les fichiers d'analyse, $g$ : grain)}

\label{algo:stocker}
\end{algorithm}

Une annotation est définie par son type, sa valeur (généralement une chaîne de caractère ou un entier), son $t$ de début et son $t$ de fin. On a donc le quadruplet (\textit{type}, \textit{valeur}, \textit{début}, \textit{fin}). L'\textbf{Algorithme \ref{algo:stocker}} détaille la formation de ce dictionnaire regroupant toutes les annotations issues de différentes sources que nous aborderons dans les sections suivantes. Pour chaque mouvement, on a une liste d'objets \textit{Annotation} qui sont eux-mêmes stockés dans un dictionnaire avec comme clef le nom du mouvement\footnote{Sous la forme "3-10-1" pour le $1^{er}$ mouvement de la $10^e$ sonate du $3^e$ opus par exemple}, on parle alors de métadonnée. La décomposition en mouvements de sonates d'Opus permet d'instaurer une arborescence dans ces métadonnées. 

\section{Annotations sur mesure $A_m$}

Nous transformons les données constatées sur partition en étiquettes et labels de notre création (voir \textsc{Figure \ref{fig:tokens}}) sur différents plans d'analyse musicale parfois ayant des fonctionnements liés, parfois indépendants. Détaillons ces différents types d'annotations.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{figures/111analyse.png}
    \caption{Analyse avec des étiquettes et labels (Opus 1, Sonate no. 1, I - Grave)}
    \label{fig:tokens}
\end{figure}

\subsection{Les métastructures $S$ et leur noms $S_n$}

Les métastructures sont des structurations à grande échelle d'un mouvement. Leur annotation permet de repérer deux parties assez proches (en les nommant avec la même lettre) voire même identifier les variations entre les 2 s'il y en a (exemple : un A+ qui désigne un A augmenté de plusieurs temps). Les lettres sont utilisées dans l'ordre alphabétique au fur et à mesure de l'apparition d'un nouveau type de métastructure pendant le mouvement.

\begin{center}
	\begin{tabular}{|c|c|c|}
	\hline
	Augmenté & Diminué & Autre variation non temporelle\\
	\hline
	\rowcolor{yellow!70}
	+ & - & $'$\\
	\hline
	\end{tabular}
\end{center}

Dans notre corpus on a très souvent au moins 1 métastructure répétée ou légèrement variée. D'autre part, on a maximum 6 annotations de métastructures différentes dans un mouvements (de A à F au maximum).

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\rowcolor{yellow!70}
	A & B & C & D & E & F\\
	\hline
	\end{tabular}
\end{center}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\columnwidth]{figures/metastructure.png}
    \caption{Enchaînements de métastructures typiques dans le corpus}
    \label{fig:metastructure}
\end{figure}

Voici en \textsc{Figure \ref{fig:metastructure}} un schéma représentant les enchaînements de métastructures les plus répandus dans notre corpus annoté et donc les plus probables lors d'une génération indépendamment des autres types d'annotations.

\subsection{Les textures générales des métastructures $S_t$}

Chaque métastructure que nous avons annoté a également une texture \cite{huron_characterizing_1989} générale, c'est à dire une annotation de la manière dont les voix d'une pièce musicale fonctionnent 2 à 2 en complémentarité (imitations\footnote{Une imitation est une répétition d'un même motif (ou dérivé de ce même motif) souvent à une hauteur différente et sur une autre voix juste après ou pendant le motif initial.}), en indépendance ou en interdépendance (homorythmies voire parallélismes \cite{giraud_towards_2014}). Dans notre cas on distingue les métastructures annotées d'une texture "$1$" qui représentent une forte présence d'homorythmies, de sauts d'octaves près des cadences avec plus généralement des retards\footnote{Un retard est une méthode de création d'une dissonance par prolongation d'une note.} 7-6 et 4-3. On a aussi des textures pour des imitations (entre 1 et 3 reproductions généralement) certaines annotées "$2a$" avec beaucoup de retards (9-8, 7-6 et 4-3) pour un motif généralement conçu comme tel, et d'autres annotées "$2b$" pour des imitations entraînant peu ou pas de retards.

\begin{center}
	\begin{tabular}{|c|c|c|}
	\hline
	Homorythmies & Imitations (avec retards) & Imitations (sans/peu de retards)\\
	\hline
	\rowcolor{Green!80}
	1 & 2a & 2b\\
	\hline
	\end{tabular}
\end{center}

Il arrive qu'une métastructure ait 2 textures différentes souvent dans chacune de ses moitiés, on symbolise ces textures hybrides en les disposant de chaque côté d'un tiret, leur ordre représentant leur apparition temporelle au cours de la métastructure.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|}
	\hline
	\rowcolor{Green!80}
	1 & 2a & 2b & 1-2a & 1-2b & 2a-1 & 2b-1\\
	\hline
	\end{tabular}
\end{center}

On construit la \textsc{Figure \ref{fig:texture}} pour résumer les enchaînements typiques de texture de métastructures (les blocs représentent une alternance entre les différents éléments). On remarque que les textures de métastructures avec imitations sont généralement du même type au cours d'un même mouvement (soit avec peu de retards soit avec beaucoup). Cela vient du fait que le même motif (ou ses variations) reste impliqué dans ces imitations.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\columnwidth]{figures/texture.png}
    \caption{Enchaînements de textures de métastructures typiques dans le corpus}
    \label{fig:texture}
\end{figure}

\subsection{Les textures liées aux mouvements mélodiques $V$}

Ces annotations sont légèrement plus bas niveau que les précédentes, cela va permettre de détailler quelles voix sont impliquées dans les homorythmies, les mouvements parallèles les imitations et de quelle manière.

\begin{center}
	\begin{tabular}{|c|c|c|}
	\hline
	Homorythmies & Parallélismes & Imitations\\
	\hline
	\rowcolor{Green!80}
	h & p & i\\
	\hline
	\end{tabular}
\end{center}

Pour les homorythmies et les parallélismes, on indique après la lettre caractérisant le mouvement mélodique les voix impliquées dans l'ordre croissant (exemple : "$h23$" pour une homorythmie impliquant les voix 2 et 3). Pour les imitations, $i[2][13]$ désigne le fait qu'un motif est énoncé par la voix 2 et la réponse a lieu simultanément sur les voix 1 et 3.

\subsection{Les tonalités locales mais marquées $T$}

Ces changements de tonalités \cite{howes_tonality_2021} au cours du morceau sont appelées modulations (ou \textit{tonicizations}). L'idée de cette annotation est évoquée dans \cite{deguernel_personalizing_2022}, on rassemble des successions harmoniques dans une structure modulante plus haut niveau. Elles sont généralement dans des tons voisins c'est à dire proche harmoniquement, on alterne donc entre la tonique et ces tons voisins afin de former une structuration harmonique. On cherche à annoter par rapport à la tonalité principale du morceau afin d'avoir la fonction harmonique plutôt que le nom de la tonalité en cours et ainsi pouvoir comparer ces structurations entre les mouvements.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	Tonique & Sus-tonique & Médiante & Sous-dominante & Dominante & Sus-dominante\\
	\hline
	\rowcolor{violet!70}
	I & II & III/rel & IV & V & VI/rel\\
	\hline
	\end{tabular}
\end{center}

Le passage dans la tonalité du III$^e$ degré par rapport à la tonalité principale du mouvement désigne généralement le relatif mineur dans une tonalité majeure et le passage au VI$^e$ degré dans une tonalité majeure témoigne souvent d'un passage au relatif majeur. Cette méthode de modulation au relatif (dans l'autre mode) est assez utilisée dans ce style afin de changer l'atmosphère et apporter des éléments contrastants.\\

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\columnwidth]{figures/tonalites.png}
    \caption{Enchaînements de tonalités marquées typiques dans le corpus}
    \label{fig:tonalites}
\end{figure}

Les enchaînements typiques des tonalités principales des zones modulantes sont agrégés dans la \textsc{Figure \ref{fig:tonalites}}. On commence et on fini dans la majorité des cas par la tonalité principale (celle de la tonique). On passe ensuite généralement par celle de la dominante et/ou celle du VI$^e$ dans le cas d'une tonalité dans la mode majeur. Bien sûr cela est le cas général, il arrive de passer dans les tons des II$^e$ et IV$^e$ degrés.

\subsection{Les cadences $C$}

Le cadences sont des caractéristiques harmoniques de la fin d'une phrase \cite{rothstein_transformations_2006}. Il peut y avoir plusieurs phrases dans une même métastructure mais il y en a donc une au moins à la fin de chaque métastructure. On peut les rassembler en 3 catégories, les cadences conclusives, les suspensives et les hybrides.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|}
	\hline
	Parfaite & Imparfaite & Demie & Phrygienne & Rompue/évitée\\
	\hline
	\rowcolor{magenta!90}
	PAC & IAC & HC & PHC & EC\\
	\hline
	V-I & V-I (6) & V & IV-V (mineur) & V-VI\\
	\hline
	conclusive & hybride & suspensive & hybride & suspensive\\
	\hline
	\end{tabular}
\end{center}

La cadence parfaite (notée PAC) est la plus présente dans le corpus, elle conclue la plupart des métastructures et donc les phrases musicales en résolvant les éventuelles tensions. Le second type de cadence le plus présent est la phrygienne, uniquement présente dans les tonalités mineures. Elle est à la fois conclusive car elle résout une tension harmonique et suspensive car elle se finit sur un V$^e$ degré mais marque également à sa manière la fin d'une phrase.
 
\subsection{Les densités rythmiques majoritaires $D$}

Cette annotation désigne le ou les 2 grains rythmiques majoritaires dans chaque mesure du mouvement. Pour symboliser la présence de 2 grains majoritaires, on les sépare par un "$/$" et on les mets dans l'ordre croissant. On a remarqué que généralement le grain rythmique majoritaire accélère à l'approche d'une cadence puis ralentit au moment de l'arrivée sur la cadence avant de reprendre ensuite.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|}
	\hline
	Ronde (4 tps.) & Blanche (2 tps.) & Noire (1 tps.) & Croche ($\frac{1}{2}$ tps.) & Double ($\frac{1}{4}$ tps.)\\
	\hline
	\rowcolor{blue!70}
	1 & 2 & 4 & 8 & 16\\
	\hline
	\end{tabular}
\end{center}

\subsection{Les motifs $M$}

Ces annotations désignent les motifs répétés au moins deux fois et de manière claire au cours du mouvement. Si un motif est impliqué dans au moins une imitation au cours du mouvement ($M_i$), il aura un label d'une lettre de la fin de l'alphabet. S'il s'agit d'un motif répété mais non imité il s'agira d'une étiquette du début de l'alphabet ($M_a$). Pour des raisons de simplification, on annote de manière similaire un motif et une variation de ce motif. On annote également tous les sauts d'octaves $M_o$ vers le haut ("$o$$<$") et vers le bas ("$o$$>$") sur les 3 voix indépendamment.

{\setlength{\tabcolsep}{1.35em}
\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}
	\hline
	\multicolumn{8}{|c|}{Motifs d'imitation} & \multicolumn{4}{|c|}{Autre motifs récurrents}\\
	\hline
	\rowcolor{orange!90}
	x & y & z & t & u & v & w & s & a & b & c & d\\
	\hline
	\end{tabular}
\end{center}}

\begin{center}
	\begin{tabular}{|c|c|}
	\hline
	Saut d'octave vers le bas & Saut d'octave vers le haut\\
	\hline
	\rowcolor{orange!90}
	$o$$>$ & $o$$<$\\
	\hline
	\end{tabular}
\end{center}

\subsection{Comportement combiné des annotations liées aux métastructures}

On a rassemblé en un seul schéma (\textsc{Figure \ref{fig:3structure}}) le comportement typique des parties métastructurantes en combinant les informations sur leur nom, leur tonalité générale ainsi que leur texture majoritaire.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{figures/3structure.png}
    \caption{Enchaînements de squelettes hybrides de structures, textures et tonalités typiques dans le corpus}
    \label{fig:3structure}
\end{figure}

\subsection{Report de nos annotations sur la plateforme Dezrann}

On combinant toutes ces annotations sur une partition, on obtient la \textsc{Figure \ref{fig:dez}} qui permet de mettre en évidence graphiquement tout ce qu'on a évoqué précédemment comme éléments d'annotations en plus de pouvoir permettre une exportation sous la forme d'un fichier .dez (fichier .json propre à Dezrann \cite{giraud_dezrann_2018}). Les données ainsi structurées peuvent être utilisées par un programme informatique.\\

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\columnwidth]{figures/111dez1.png}
    \hfill{\vspace{10pt}}
    \includegraphics[width=\columnwidth]{figures/111dez2.png}
    \caption{Retranscription sur Dezrann des annotations effectuées sur un mouvement de sonate de Corelli (Opus 1, Sonate no. 1, I - Grave)}
    \label{fig:dez}
\end{figure}

Nous transcrivons alors ces données d'annotations sur la plateforme Dezrann en les rassemblant sur \url{http://www.dezrann.net/explore/corelli-trio-sonatas}. Il s'agit d'un corpus rassemblant les 40 mouvements lents de sonates en trio dites d'église d'Arcangelo Corelli extraits des Opus 1 (1681) et 3 (1689). 


\section{Annotations de DCMLab $A_d$}

Dans le cadre de \cite{hentschel_semi-automated_2021}, DCMLab\footnote{\textit{Digital and Cognitive Musicology Lab} de l'École Polytechnique Fédérale de Lausanne (EPFL)} a annoté les Opus 1, 3 et 4 des sonates en trio de Corelli avec dans le lot les 40 mouvements que nous avons annotés selon les techniques et choix qui viennent d'être évoquées. Nous avons simplifié certaines de leur annotations au sein des nôtres, laissé de côté certaines mais voici celles qui vont nous intéresser.

\subsection{Fonction harmonique $H$ détaillée de chaque accord successif}

La fonction harmonique de chaque accord (empilement de notes) en rapport avec la tonique de la zone tonale en cours est une donnée intéressante. La transition entre ces éléments suit généralement le schéma de la \textsc{Figure }\ref{fig:roueclass}. On a ici une dimension supplémentaire qui est le mode de l'accord en question (majuscule pour majeur, minuscule pour mineur). Les degrés $H_d$ sont stockés sous la forme de chiffres romains et respectent la notation internationale.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
	\hline
	\rowcolor{black!20}
	I & i & II & ii & III & iii & IV & iv & V & v & VI & vi & VII & vii\\
	\hline
	\end{tabular}
\end{center}

Une autre composante de la fonction harmonique d'un accord est la disposition de l'empilement de chacune des 3 notes d'un accord (puisque 3 voix) en lien avec leur fonction inhérente à la tonique d'accord. On prend ces données sous la forme d'une notation en chiffre arabes utilisée par les musicologues qui a ces caractéristiques là tout en étant assez proche de la représentation d'un GCT (voir \cite{meredith_harmonic_2016} et comme évoqué dans le chapitre précédent). On appelle cela un chiffrage d'accord $H_c$.

\section{Annotations liées à la partition $A_p$ et aux notes $N$}

\subsection{Hauteur $N_h$, durée des notes $N_d$, position sur les voix $N_v$ et dans le temps}

À chaque fréquence correspond une hauteur de son. Les musiciens ont inventé une dénomination pour faciliter la transmission des informations de hauteur de son, chaque note est placée sur une portée et la clef en début de portée désigne de quelle tessiture de notes il s'agit (\textsc{Figure }\ref{fig:note_freq}).

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{figures/note_freq.png}
    \caption{Association fréquence/note sur une portée en fonction de la clef de Sol (en haut) ou Fa (en bas)}
    \label{fig:note_freq}
\end{figure}

Dans la musique occidentale, on dénombre 12 de ces hauteurs de notes (\textsc{Figure }\ref{fig:note_nom}) ; on a donc 12 noms de notes successives (de Do à Si) séparées par un intervalle appellé demi-ton. Il n'existe pas seulement 12 notes différentes mais beaucoup plus, c'est pourquoi on répète cette succession de 12 notes. À la suite du nom de la note et pour l'identifier précisément, on utilise un chiffre qui s'appelle le numéro d'octave pour différencier deux notes ayant le même nom, 2 notes séparées par une octave étant distantes de 12 demis-tons\footnote{La fréquence est doublée pour 2 notes de même nom séparées par une octave.}. Le changement d'octave se fait à partir du Do. Par exemple, un Si4 et un Do5 sont deux notes successives.\\

\vspace{-4pt}

\begin{figure}[htbp]
\tiny
    \centering
    \begin{tabular}{|>{\columncolor{gray}}c|c|>{\columncolor{black},\color{white}}c|c|>{\columncolor{black},\color{white}}c|c|c|>{\columncolor{black},\color{white}}c|c|>{\columncolor{black},\color{white}}c|c|>{\columncolor{black},\color{white}}c|c|}
    \hline
    \textbf{Nom} & \textbf{Do} & \textbf{Do$\sharp$/Ré$\flat$} & \textbf{Ré} & \textbf{Ré$\sharp$/Mi$\flat$} & \textbf{Mi} & \textbf{Fa} & \textbf{Fa$\sharp$/Sol$\flat$} & \textbf{Sol} & \textbf{Sol$\sharp$/La$\flat$} & \textbf{La} & \textbf{La$\sharp$/Si$\flat$} & \textbf{Si} \\
    \hline
    Notation & C & C+/D- & D & D+/E- & E & F & F+/G- & G & G+/A- & A & A+/B- & B \\
    \hline
    Indice & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 \\
    \hline
    \end{tabular}
    \caption{Nom des notes dans la musique occidentale et rapport avec la programmation}
    \label{fig:note_nom}
\end{figure}

La variable que l'on utilise pour stocker une hauteur de note est un entier $H$ défini comme :
$$H = 12.(n_{octave}+1) + id_{note}$$
$$\text{ (avec } n_{octave} \text{ le numéro d'octave et } id_{note} \text{ l'indice de la note)}$$

On a donc les propriétés suivantes :
$$id_{note} = H\text{ mod }12 \quad\quad\text{ et }\quad\quad n_{octave} = \left\lfloor\frac{H}{12}\right\rfloor-1$$

On choisit de stocker l'ensemble des positions temporelles des notes des 3 voix (donnée flottante) ainsi que leur durée (en nombre de temps) et la hauteur en nombre de demi-tons (de laquelle on peut extraire l'octave et l'indice de la note). Voici en \textsc{Figure }\ref{fig:midibert} un exemple de représentation de ces informations sur des notes (\textit{sub-beat} : position temporelle de la note en nombre de 1/4 de temps, \textit{pitch} : hauteur en nombre de demi-tons, \textit{duration} : durée en nombre de temps). On parle de \textit{tokenisation}.

\newpage

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/midibert.png}
    \caption{Exemple d'une méthode encodage des notes en \textit{tokens} (MidiBERT-Piano \cite{chou_midibert-piano_2021})}
    \label{fig:midibert}
\end{figure}

\section{Aide à la décision sur la priorité des annotations}

Il faut faire des choix d'ordres de priorité de toutes ces annotations lors de la conception de nos modèles.

On considère tout d'abord que nos annotations sur mesure devraient avoir une priorité plus importante car elles ont été conçues spécifiquement pour résoudre le problème. De plus, les données concernant les notes sont trop précises comparé à des annotations effectuées par un humain. De plus, la fonction harmonique d'un accord (un groupe de notes empilées) est plus importante que des notes organisées de manières aléatoires, on a donc :
$$A_m\succ A_d\succ A_p$$
De manière générale, on sait que du fait du premier niveau d'arborescence des annotations, on a :
$$A_m = S \wedge V \wedge T \wedge C \wedge D \wedge M$$
$$A_d = H$$
$$A_p = N$$
Pour autant, au sein des annotations sur mesure $A_m$, on a la hiérarchie suivante basée notamment sur la taille moyenne de la zone temporelle que cela représente combiné à d'autres facteurs musicaux comme le lien qu'elle ont entre elles :
$$S \approx C \succ T \succ V \succ M \succeq D$$
On a donc :
$$S \approx C \succ T \succ V \succ M \succeq D \succ H \succ N$$
En descendant dans l'arborescence hiérarchique de ces annotations et en procédant à de nouvelles analyses des préférences on a :
$$S=S_n \wedge S_t \text{ avec }S_n \succ S_t$$
car le nom d'une métastructure donne une information de plus haut niveau que sa texture,
$$M=M_i \wedge M_a \wedge M_o \text{ avec }M_o \succeq M_i \succ M_a$$
car un saut d'octave et un motif d'imitation ont un effet plus grand qu'un simple motif répété,
$$H = H_d \wedge H_c \text{ avec } H_d \succ H_c$$
parce que le degré a un plus grand rôle dans la fonction harmonique d'un accord que l'ordre des notes au sein de cet accord et
$$N = N_v \wedge N_h \wedge N_d \text{ avec } N_h \approx N_d \succ N_v $$
car la hauteur et la durée d'une note cohabitent et forment la même entité tandis que la voix sur laquelle est cette note reste assez accessoire tant le timbre des instruments est proche voire indissociable.\\

Le but d'avoir cet ordre complet des préférences des annotations va nous permettre de choisir de manière plus raisonnée les poids que vont prendre telle ou telle annotation dans la prise de décision du modèle. Par exemple, ce poids d'apprentissage sera plus haut dans le cas d'annotations liées aux métastructures que celle des notes. Pour autant, on sait qu'il existe des liens entre certaines annotations assez éloignées dans l'ordre global et ce sera l'occasion de mettre en évidence ces paradoxes.
