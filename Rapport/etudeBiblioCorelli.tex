\documentclass{article}
\usepackage{Template_JIM/jim,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{gensymb}

\usepackage{enumerate}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{amsfonts}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{sidecap}
\sidecaptionvpos{figure}{c}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage[justification=centering]{caption}
\usepackage{colortbl}
\definecolor{green}{rgb}{0.0, 0.5, 0.0}
\usepackage{array,multirow}
\usepackage[french,frenchkw,boxed,ruled,lined]{algorithm2e}
\SetKw{KwDe}{de}
\SetKw{KwOu}{ou}
\SetKw{KwEt}{et}
\SetKw{Kwrenv}{renvoyer}
\SetKwInput{Variables}{Variables}
\SetKwInput{Variable}{Variable}
\usepackage{xifthen}
\usepackage{colortbl}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{comment}
\usepackage{todonotes}
\usepgfplotslibrary{statistics}
\pgfplotsset{compat=1.8}
\pgfmathdeclarefunction{fpumod}{2}{%
    \pgfmathfloatdivide{#1}{#2}%
    \pgfmathfloatint{\pgfmathresult}%
    \pgfmathfloatmultiply{\pgfmathresult}{#2}%
    \pgfmathfloatsubtract{#1}{\pgfmathresult}%
    \pgfmathfloatifapproxequalrel{\pgfmathresult}{#2}{\def\pgfmathresult{3}}{}%
}

\newcommand{\titlebox}[2]{%
\tikzstyle{titlebox}=[rectangle,inner sep=10pt,inner ysep=10pt,draw]%
\tikzstyle{title}=[fill=white]%
%
\bigskip\noindent\begin{tikzpicture}
\node[titlebox] (box){%
    \begin{minipage}{0.94\textwidth}
#2
    \end{minipage}
};
%\draw (box.north west)--(box.north east);
\node[title] at (box.north) {#1};
\end{tikzpicture}\bigskip%
}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\tiny,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    frame=single,
}

\title{Étude bibliographique et généralités - Génération procédurale de l'écriture en trio d'après Corelli}

\oneauthor
  {Jérémie Roux} {Université de Montpellier - Faculté des Sciences (M2 Informatique IA-SD)\\ jeremie.roux@etu.umontpellier.fr}


\begin{document}

\maketitle

\section{État de l'art}

On s'intéressera ici à la génération d'un schéma (ou modèle) tonal d'écriture en trio et sa redistribution \cite{tonalModel} au sein d'un mouvement de sonate en trio de Corelli généré procéduralement. La forme sonate est une propriété macroscopique de l'œuvre qu'il conviendra de traiter plus tard. En effet, Corelli a composé dans sa vie un peu plus de 5 opus de 12 sonates en trio\footnote{4 mouvements chacunes} (sa forme de prédilection avec notamment l'exploitation du thème de \textit{La Follia} \cite{dossierLaFollia}) dans lesquelles cette écriture est utilisée\footnote{Le reste de son répertoire étant principalement des \textit{Concerti grossi} qui font chacun dialoguer des petits groupes de solistes avec l'orchestre}. Toujours dans \cite{tonalModel}, Wintle évoque des changements de tonalités (passages au relatif, à l'homonyme ou autres tons voisins), des répétitions et des expansions/prolongations de modèles tonaux comme une manière de développer des éléments et structurer un mouvement. La génération de thèmes qui pourront être fugués \cite{roux} grâce à l'utilisation de procédés d'imitations permet d'améliorer la richesse de l'écriture.\\

Un ensemble de 36 sonates en trio de Corelli a déjà été annoté\footnote{\url{https://github.com/DCMLab/corelli}} (cadences, phrases, harmonies) et présenté dans \cite{annotationCorelli}. Cela peut être utilisé pour générer des modèles de Markov en calculant des probabilités de transition de tel degré vers tel degré de la tonalité en cours, de renversements, de modulation à d'autres tonalités (et lesquelles) ainsi que d'autres propriétés calculées sur l'entièreté ou partie de ce corpus. Il existe déjà des approches de génération de musique baroque comme \textit{DeepBach} \cite{deepbach} qui le fait avec des chorals de Bach en utilisant l'apprentissage. On recherche plutôt ici une approche note à note au début, puis par structure et macro structure ensuite. On commencera par l'implémentation des règles musicales de Corelli, ce qui passe obligatoirement par la compréhension de ces règles et de ce langage harmonique spécifique. On passera ensuite par de l'apprentissage pour construire des structures plus abouties et inspirées du corpus disponible.

\section{Corelli, un compositeur influent}

Arcangelo Corelli (1653-1713), né à Fusignano et mort à Rome\footnote{Ces deux lieux se situaient alors dans les États pontificaux sous l'autorité du pape.}, est un violoniste et compositeur majeur de la période baroque (voir \textbf{Figure \ref{fig:corelli}}). Il pourrait avoir voyagé en Europe\footnote{En France, en Espagne, en Allemagne mais aucun document ne le prouve véritablement} avant de s'installer définitivement à Rome où il passa presque tout le reste de sa vie, ne la quittant que pour un voyage à Naples.\\

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\columnwidth]{figures/corelli}
    \caption{Portrait d'Arcangelo Corelli par Johan Francois Douven}
    \label{fig:corelli}
\end{figure}

Son influence a été très grande, à la fois dans la diffusion de formes nouvelles (sonate et concerto grosso) et dans la technique du violon. Ce rayonnement est exercé largement au-delà des frontières italiennes par des contemporains légèrement plus jeunes que lui comme J. S. Bach (1685-1750), François Couperin (1668-1733) ou J. F. Dandrieu (1682-1738) qui l'admiraient beaucoup et ont marqué certaines de leurs compositions par son empreinte. Le premier lui a emprunté un thème de sa sonate d'église (\textit{da chiesa}) en trio op.3 n\degre4 pour sa fugue d'orgue en si mineur (BWV 579). Le second lui dédia l'\textit{Apothéose de Corelli} et s'efforça d'imitier son style dans les sonates dites des \textit{Goûts-réunis} (ou \textit{Nouveaux Concerts}) publiés en 1724 dont fait partie cette Apothéose. Le troisième intitula l’une de ses pièces pour clavecin \textit{La Corelli}, il atteste ainsi son souhait de s’imposer comme l’héritier du grand maître italien \cite{dandrieu}.\\

Si elle inspirait les musiciens d'alors et même d'aujourd'hui ayant compris l'influence qu'elle a eu dans l'histoire musicale, la musique de Corelli ne faisait cependant pas l'unanimité dans le grand public et en dehors du cercle religieux comme par exemple au 19$^e$ siècle où certains évoquent cela comme "une antiquité intéressante dans un style strict" mais aussi "une antiquité plus curieuse qu'adaptée à une salle de concert" \cite{performingCorelli}.

\section{Effectif instrumental}

L'écriture de Corelli \cite{wavelet} utilise le principe de composition à deux dessus et une basse continue. En conséquence, les deux instruments aïgus sont égaux dans leur tessiture et leur rôle mélodique. Les croisements entre eux sont fréquents.\\

Corelli étant violoniste, ses œuvres sont destinées aux cordes (2 violons et une basse continue\footnote{Une basse continue ou \textit{continuo} accompagne les autres voix en improvisant sur l'harmonie qui lui est donnée. Elle est constituée d'un ou plusieurs instruments monodiques graves (violoncelle, viole de gambe, basson) qui jouent la ligne de basse écrite, et un ou plusieurs instruments harmoniques (clavecin, orgue, théorbe, luth, guitare baroque) qui réalisent, c'est-à-dire qui complètent l'harmonie. (Wikipédia)}) mais d'autres instruments mélodiques peuvent interpréter cette musique dès lors que leur tessiture couvre celle du morceau. 

\section{Langage harmonique et réalisation}

Le but de cette écriture vise à enchaîner des phrases ponctuées et séparées par des cadences\footnote{Types et emplacements de cadences qu'il conviendra d'étudier plus précisément}. La dimension harmonique sera ici plus importante que la dimension mélodique, la place du thème étant assez peu importante comparée à une fugue et son sujet par exemple. Toutefois, on pourra retrouver certains éléments rappelés ou variés (rythmiquement et mélodiquement) au fur et à mesure de la musique, ce qui aidera notamment à sa structuration.\\

La présence de la réalisation de la basse continue ne suffit pas à remplir l'harmonie. Le trio doit sonner agréablement par lui-même, on s'efforcera donc autant que possible de compléter les harmonies à 3 sons et répartir les notes des accords choisis sur les 3 voix de façon à ce que les voix soient le plus souvent peu mobiles. Dans \cite{tonalModel}, Wintle parle de modèle tonal (succession d'accords avec potentiellement des renversements mais sur la même octave) et de redistribution de ce modèle lorsque l'on cherche à éclater ces accords et répartir les notes sur les 3 voix intelligemment. Pour les harmonies à 4 sons, il faut généralement se dispenser de la quinte, sauf si une nécessité de préparation de dissonance impose de la faire entendre au détriment de la tierce.\\

Corelli utilise tous les accords à 3 et 4 sons, passons donc en revue les plus connus, leurs fonctions, les renversements utilisés ou non par Corelli dans ce style en trio ainsi que leurs notations (en chiffres arabes) et les règles associées \cite{wavelet}.

\subsection{Accord parfait}

L'accord parfait est le plus connu et basique des accords à 3 sons. Le premier renversement est d'une nature plus légère que l'état fondamental. On ne double pas la basse de cet accord dit de sixte sauf sur le II$^e$ degré de la gamme. L'usage de la sixte et quarte (2$^e$ renversement) est rare dans ce style, uniquement en sixte et quarte de cadence.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/accord_parfait}
    \caption{Les différents renversements de l'accord parfait}
    \label{fig:parfait}
\end{figure}

\subsection{Accords de septièmes}

Les accords de septièmes de dominante (voir \textbf{Figure \ref{fig:7dom}}) et d'espèce (voir \textbf{Figure \ref{fig:7}}) s'utilisent à l'état fondamental, aux 1$^{er}$ et 3$^e$ renversements, le 2$^e$ renversement est à exclure. Toutes les septièmes sont traitées comme des dissonances nécessitant préparation et résolution.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/septieme_dominante}
    \caption{Les différents renversements de l'accord de septième de dominante}
    \label{fig:7dom}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/septieme}
    \caption{Les différents renversements de l'accord de septième d'espèce}
    \label{fig:7}
\end{figure}

\subsection{Accord de quinte diminuée}

L'accord de quinte diminuée (sur la sensible) est à l'état fondamental un empilement de tierces mineures. Il est basé sur un accord de septième de dominante auquel on a retiré la fondamentale (d'où la notation \textit{V sf}) et peut être utilisé dans tous ses renversements (voir \textbf{Figure \ref{fig:5dim}}). La quarte issue du second renversement est une quarte augmentée.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/quinte_diminuee}
    \caption{Les différents renversements de l'accord de quinte diminuée}
    \label{fig:5dim}
\end{figure}

\section{Usage des retards}

Dans l'écriture en trio, la présence de nombreuses dissonances notamment grâce à des retards est essentielle. Ils s'effectuent toujours vers le bas, la musique aura donc pour particularité de descendre dans les graves et on profitera de la fin de la phrase, d'une cadence pour pouvoir remonter à la bonne hauteur et ainsi ne pas sortir de la tessiture des instruments. Une préparation doit toujours précéder une résolution et la durée d'un retard doit être inférieure ou égale à la durée de sa préparation\footnote{Durée pendant laquelle la note remplaçant celle retardée est tenue avant la réalisation du retard en lui-même (notes liées dont la première est identique à la préparation)}. Il ne faut pas faire entendre la note retardée en même temps que le retard\footnote{Sauf dans le cas du retard 9-8}. Il peut arriver que le retard se résolve au moment d'un changement de basse. Les doubles retards superposés ne sont pas utilisés dans l'écriture en trio mais on peut cependant les faire se succéder (la résolution de l'un étant la préparation de l'autre) en faisant ou non alterner le dessus sur lesquels ils sont réalisés.\\

Les dissonances sont mieux mises en valeur en étant entendues à la partie supérieure mais on pourra disposer les retards sur les deux dessus que l'on écrira après la basse car leur réalisation en est dépendante. La résolution des dissonances peut faire l'objet d'une ornementation mélodique. Étudions les 3 types de retards que l'on rencontre chez Corelli.

\subsection{Retard 4-3}

Le retard 4-3 (voir \textbf{Figure \ref{fig:ret43}}) s'effectue sur un accord parfait à l'état fondamental, c'est le retard de la tierce. Il est ici réalisé avec la préparation du retard sur le degré I et le retard sur le degré V, on peut également effectuer cela en majeur et/ou sur un enchainement \textit{IV - I} par exemple et/ou sur un \textit{V sf - I} avec une quinte diminuée sur la préparation.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/retard43.png}
    \caption{Exemples de retards 4-3}
    \label{fig:ret43}
\end{figure}

\subsection{Retard 7-6}

Le retard 7-6 (voir \textbf{Figure \ref{fig:ret76}}) s'effectue sur le premier renversement d'un accord de trois sons. Le chiffrage 7 suivi du 6 n'indique pas une septième d'espèce, il s'agit d'un accord 6 dont la sixte est retardée. Il est ici réalisé avec la préparation du retard sur le degré V et le retard sur le degré IV mais il y a d'autres possibilités.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/retard76.png}
    \caption{Exemples de retards 7-6}
    \label{fig:ret76}
\end{figure}

\subsection{Retard 9-8}

Le retard 9-8 (voir \textbf{Figure \ref{fig:ret98}}) s'effectue sur l'accord parfait à l'état fondamental, c'est le retard de la fondamentale. Le retard doit se situer à au moins une neuvième de la basse. Il est ici réalisé avec la préparation du retard sur le degré V et le retard sur le degré I mais il y a d'autres possibilités.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/retard98.png}
    \caption{Exemples de retards 9-8}
    \label{fig:ret98}
\end{figure}

\section{Usage des imitations}

En contrepoint, on tient toujours compte de l'harmonie. Chaque note doit être réelle ou une note étrangère identifiable (broderie, note de passage, retard). L'imitation est un procédé d'écriture polyphonique qui consiste à reproduire plus ou moins fidèlement dans une voix et à un intervalle de transposition quelconque une idée mélodique prise dans une autre voix. L'idée est appelée "antécédent" et sa reproduction est appelée "conséquent". Ces imitations pourront être effectuées sur les 3 voix.\\

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/mutations.png}
    \caption{Exemples d'imitations avec et sans mutation}
    \label{fig:imit}
\end{figure}

On pourra utiliser des imitations strictes (canon) à l'unisson, à l'octave, aux quartes ou quintes supérieures ou inférieures. On peut également réaliser des mutations intervalliques ou rythmiques dans l'imitation (voir \textbf{Figure \ref{fig:imit}}). On peut combiner un intervalle d'imitation avec une ou plusieurs mutations intervalliques ou rythmiques entre l'antécédent et le conséquent.

\section{Conclusion}

Le but de ce travail est de générer de la musique et plus particulièrement cette écriture en trio de Corelli basée sur un langage harmonique bien spécifique ou s'expriment ces deux dessus et cette basse continue avec notamment des retards, des cadences et des imitations.\\

Au niveau de l'enchaînement harmonique (ou schéma tonal), il conviendra d'établir une roue des degrés (sous forme d'un Modèle de Markov) un peu à la manière de celle qui existe en musique classique au sein de la tonalité en cours (voir \textbf{Figure \ref{fig:roueclass}}), ainsi que les différents moyens de moduler à des tons voisins, via des dominantes secondaires notamment. Au niveau rythmique et mélodique, il faudra rester simple pour faire ressortir les imitations. On commencera sans doute par de l'harmonisation automatique d'une basse existante (sans chiffrage) pour ensuite passer à de la génération complète sans imitation au début puis avec imitations ensuite. On apprendra également sur le corpus pour générer des structures harmoniques (ou modèles tonaux) qui s'en inspirent.\\

\begin{figure}[h!]
    \centering
    \includegraphics[width=\columnwidth]{figures/roue.png}
    \caption{Roue des degrés dans le style classique (extrait du cours de Vincent Wavelet \cite{wavelet})}
    \label{fig:roueclass}
\end{figure}

L'objectif est de démontrer qu'il est possible de générer une musique structurée en implémentant les règles dictées par les compositeurs et en espérant obtenir un produit d'une meilleure qualité musicale qu'en utilisant seulement de l'apprentissage : les règles implémentées sont celles enseignées en classe d'écriture au conservatoire.\\

Cela pourrait être utile pour créer par exemple de la musique libre de droit pour l'usage d'autres études scientifiques. Il y aurait également un intérêt artistique voire historique de savoir ce que Corelli aurait pu faire d'autre dans son style (le nombre d'oeuvres perdues avec le temps étant conséquent). Enfin, et cette utilité là est assez spécifique, on pourrait générer cette musique dans le but d'ensuite supprimer les deux dessus et créer des exercices d'harmonisation sur des basses continues inédites pour éviter la triche ou la copie d'une oeuvre existante dans le cadre d'un examen au conservatoire par exemple.

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}
