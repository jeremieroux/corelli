from transformers import pipeline
import wikipediaapi

# Dictionnary of the musical periods
P = {
    "medieval": "(1150 – 1400)",
    "renaissance": "(1400 – 1600)",
    "baroque": "(1600 – 1750)",
    "classical": "(1750 – 1820)",
    "romantic": "(1820 – 1900)"
}

# For a summary, add the dates of the musical periods
def period(summary: str):
    result = ""
    for i in summary.split():
        result += i + " "
        if i.lower() in P.keys():
            result += P[i.lower()] + " "

    return result

x = input('Choose a composer (in english) : ')
print("\n")

# Source : https://huggingface.co/facebook/bart-large-cnn
summarizer = pipeline("summarization", model="facebook/bart-large-cnn")

wiki_wiki = wikipediaapi.Wikipedia('en')
page_py = wiki_wiki.page(x)
print("SUMMARY OF THE WIKIPEDIA ARTICLE : " + page_py.summary + "\n")

resume = summarizer(page_py.summary, max_length=200, min_length=100, do_sample=False)
resume = resume[0]['summary_text']
print("SUMMARY OF THE SUMMARY : " + period(resume))