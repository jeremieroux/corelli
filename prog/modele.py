# -*- coding: utf-8 -*-
"""StageM2 Modèle

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1CJq7T8gwzoNNxoY7NLhARa7YSjzXmwL2
"""

# Installer les bibliothèques nécessaires
!pip install --upgrade pytorch-lightning

import numpy as np
import pandas as pd
import re
import sys
import json

import os
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch import Tensor
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import tensorflow as tf
import pytorch_lightning as pl
import spacy

from time import process_time
from zmq.constants import NULL

# Importer les bibliothèques nécessaires
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from torchvision.transforms import ToTensor
from sklearn.model_selection import train_test_split
import pytorch_lightning as pl

# Commented out IPython magic to ensure Python compatibility.
# Pour monter son drive Google Drive local
from google.colab import drive
drive.mount('/content/gdrive')

my_local_drive='/content/gdrive/My Drive/Colab Notebooks/StageM2'
# Ajout du path pour les librairies, fonctions et données
sys.path.append(my_local_drive)
# Se positionner sur le répertoire associé
# %cd $my_local_drive
# %pwd

# Préparer les données
class MyDataset(Dataset):
    def __init__(self, data_path):
        self.data = pd.read_csv(data_path)

    def __getitem__(self, index):
        src = self.data['t']
        tgt = self.data['degre']
        display(src)
        src = torch.DoubleTensor(src)
        tgt = torch.DoubleTensor(tgt)
        return src, tgt

    def __len__(self):
        return len(self.data)

# Définir la classe du modèle du transformeur
class TransformerModel(pl.LightningModule):
    def __init__(self, num_classes):
        super(TransformerModel, self).__init__()
        self.transformer = nn.Transformer(d_model=256, nhead=8, num_encoder_layers=6, num_decoder_layers=6, dtype=torch.float64)
        self.encoder_embedding = nn.Embedding(num_classes, 256, dtype=torch.float64)
        self.decoder_embedding = nn.Embedding(num_classes, 256, dtype=torch.float64)
        self.fc = nn.Linear(256, num_classes)

    def forward(self, src, tgt):

        src_embedding = self.encoder_embedding(src)
        tgt_embedding = self.decoder_embedding(tgt)

        output = self.transformer(src_embedding, tgt_embedding)

        output = self.fc(output)

        return output

    def training_step(self, batch, batch_idx):
        src, tgt = batch
        output = self(src, tgt)
        loss = F.cross_entropy(output.view(-1, output.size(-1)), tgt.view(-1))
        self.log('train_loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        src, tgt = batch
        output = self(src, tgt)
        loss = F.cross_entropy(output.view(-1, output.size(-1)), tgt.view(-1))
        self.log('val_loss', loss)

    def test_step(self, batch, batch_idx):
        src, tgt = batch
        output = self(src, tgt)
        loss = F.cross_entropy(output.view(-1, output.size(-1)), tgt.view(-1))
        self.log('test_loss', loss)

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=1e-3)
        return optimizer

data = pd.read_csv("one_hot_dezrann/one_hot_global.csv")

# Séparer les données en ensembles d'entraînement, de validation et de test
train_data, val_data = train_test_split(data, test_size=0.2, random_state=42)
val_data, test_data = train_test_split(val_data, test_size=0.5, random_state=42)

# Enregistrer les ensembles de données dans des fichiers CSV
train_data.to_csv('train_data.csv', index=False)
val_data.to_csv('val_data.csv', index=False)
test_data.to_csv('test_data.csv', index=False)

train_dataset = MyDataset('train_data.csv')
val_dataset = MyDataset('val_data.csv')
test_dataset = MyDataset('test_data.csv')

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=64)
test_loader = DataLoader(test_dataset, batch_size=64)

# Initialiser le modèle
model = TransformerModel(num_classes=256)

# Initialiser le formateur
trainer = pl.Trainer(max_epochs=10)

print(train_data.head())

# Entraîner le modèle
trainer.fit(model, train_loader, val_loader)

# Tester le modèle
trainer.test(model, test_loader)