import json

# Mouvements de sonate en trio annotés
mouvements = ["1-1-1", "1-1-3", "1-2-1", "1-2-3", "1-3-1", "1-3-3", "1-4-1", "1-5-1", "1-6-1", "1-6-3", "1-7-2",
              "1-8-1", "1-8-3", "1-9-3", "1-10-1", "1-10-4", "1-11-1", "1-11-3", "1-12-1", "1-12-3", "3-1-1", "3-2-1",
              "3-2-3", "3-3-1", "3-3-3", "3-4-1", "3-4-3", "3-5-1", "3-5-3", "3-6-2", "3-7-1", "3-7-3", "3-8-1",
              "3-8-3", "3-9-1", "3-9-3", "3-10-1", "3-10-3", "3-11-1", "3-11-3"]

# Dictionnaires pour transformer en nombre les annotations
dic_pattern = {"1": 0, "2a": 0, "2b": 0, "1-2a": 0, "1-2b": 0, "2a-1": 0, "2b-1": 0}
dic_struct = {"A": 0, "B": 0, "C": 0, "D": 0, "E": 0, "F": 0}
dic_cad = {"PAC": 0, "IAC": 0, "HC": 0, "PHC": 0, "EC": 0}
dic_degre = {"I": 0, "II": 0, "III": 0, "III/rel": 0, "IV": 0, "V": 0, "VI/rel": 0}
dic_texture = {"h123": 0, "p123": 0, "h12": 0, "p12": 0, "h13": 0, "p13": 0, "h23": 0, "p23": 0, "i[1][2]": 0,
               "i[1][3]": 0, "i[2][1]": 0, "i[2][3]": 0, "i[3][1]": 0, "i[3][2]": 0, "i[1][23]": 0, "i[2][13]": 0,
               "i[3][12]": 0, "i[12][3]": 0, "i[13][2]": 0, "i[23][1]": 0, "i[1][2][3]": 0}
dic_motif = {"x": 0, "y": 0, "z": 0, "t": 0, "u": 0, "v": 0, "w": 0, "s": 0, "a": 0, "b": 0, "c": 0, "d": 0, "o>": 0,
             "o<": 0}
dic_densite = {"1": 0, "1/2": 0, "1/4": 0, "1/8": 0, "2": 0, "2/4": 0, "4/2": 0, "2/16": 0, "4": 0, "4/8": 0, "4/16": 0,
               "2/8": 0, "8": 0, "8/16": 0, "16": 0}

# Récupération des données
dezrann = {}
for num in mouvements:
    dezrann[num] = json.load(
        open('../partitions_annotations_synchro/annotation_dezrann/op' + num + '.dez'))  # Annotations manuelles

for a in dezrann:
    for i in dezrann[a]["labels"]:
        if i["type"] == "Texture" and i["line"] == "top.1":  # Texture de méta-structure
            dic_pattern[i["tag"]] += 1
        elif i["type"] == "Structure" and i["line"] == "top.3":  # Méta-structure
            dic_struct[i["tag"][0]] += 1
        elif i["type"] == "Cadence":  # Cadence
            dic_cad[i["tag"]] += 1
        elif i["type"] == "Modulation" and i["line"] == "top.2":  # Degré de modulation
            dic_degre[i["tag"]] += 1
        elif i["type"] == "Pattern" and "staff" not in i and i["line"] == "bot.2":  # Densité rythmique majoritaire
            dic_densite[i["tag"]] += 1
        elif i["type"] == "Texture" and "staff" not in i and i["line"] == "bot.1":  # Texture h/p/i
            dic_texture[i["tag"]] += 1
        elif i["type"] == "Pattern" and "line" not in i:  # Motifs
            dic_motif[i["tag"]] += 1

print(dic_pattern)
print(dic_struct)
print(dic_cad)
print(dic_degre)
print(dic_densite)
print(dic_texture)
print(dic_motif)

with open('vocabulaire.csv', 'w') as f:
    for my_dict in [dic_pattern, dic_struct, dic_cad, dic_degre, dic_densite, dic_texture, dic_motif]:
        for key in my_dict.keys():
            f.write("%s,%s\n" % (key, my_dict[key]))