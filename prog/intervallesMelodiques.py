import os
import ms3
import matplotlib.pyplot as plt
import numpy as np

dossier = 'notes'  # Dossier dans lequel sont les données
intervalles = [[], [], []]  # Intervalles dans chaque voix

def calculIntervalles(chemin_vers_fichier:str):
    notes = ms3.load_tsv(chemin_vers_fichier).filter(items=['staff', 'midi'])  # Récupération des colonnes intéressantes
    notes = notes[notes['staff'] < 4]  # Conservation d'une seule basse continue
    notes = notes.reset_index()  # Mise à jour des index

    last = [-1, -1, -1]  # Dernière note de chaque voix
    for index, ligne in notes.iterrows():  # Pour chaque ligne du dataframe
        if last[ligne['staff'] - 1] != -1:  # Si ce n'est pas la première note de la voix dans le mouvement
            intervalles[ligne['staff'] - 1].append(
                ligne['midi'] - last[ligne['staff'] - 1])  # Ajout de l'intervalle mélodique
        last[ligne['staff'] - 1] = ligne['midi']  # Mise à jour de la dernière note de la voix

for fichier in os.listdir(dossier):
    f = os.path.join(dossier, fichier)
    if os.path.isfile(f) and fichier[0]=='o':
        #print('Traitement de ',f)
        calculIntervalles(f)

#print(intervalles)

# Histogramme
plt.hist(intervalles, density=True, bins=np.linspace(-25, 25),label=['Violon I','Violon II','Basse continue'])  # density=False would make counts
plt.ylabel('Probabilité')
plt.xlabel('Intervalle mélodique (en nombre de demi-tons)')
plt.title('Répartition des intervalles mélodiques sur chacune des voix des sonates en trio d\'Arcangello Corelli (Opus 1-3-4)')
plt.legend(loc='upper right')
plt.show()