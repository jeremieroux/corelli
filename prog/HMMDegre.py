import json
import pandas as pd
import numpy as np

# Mouvements de sonate en trio annotés
mouvements = ["1-1-1","1-1-3","1-2-1","1-2-3","1-3-1","1-3-3","1-4-1","1-5-1","1-6-1","1-6-3","1-7-2","1-8-1","1-8-3","1-9-3","1-10-1","1-10-4","1-11-1","1-11-3","1-12-1","1-12-3","3-1-1","3-2-1","3-2-3","3-3-1","3-3-3","3-4-1","3-4-3","3-5-1","3-5-3","3-6-2","3-7-1","3-7-3","3-8-1","3-8-3","3-9-1","3-9-3","3-10-1","3-10-3","3-11-1","3-11-3"]

# Transformation des noms 1-1-1 en op01n01a (syntaxe DCMLab)
dezrann2 = {}
dict_lettre={"1":"a","2":"b","3":"c","4":"d"}
for a in mouvements :
  s = a.split("-")
  result = "op"

  if(len(s[0])==1):
    result+="0"
  result+=s[0]

  result+= "n"
  if(len(s[1])==1):
    result+="0"
  result+=s[1]

  result+=dict_lettre[s[2]]
  dezrann2[a]=result

dic_deg={"I":1,"i":1,"II":2,"bII":2,"ii":2,"III":3,"iii":3,"IV":4,"iv":4,"V":5,"v":5,"VI":6,"bVI":6,"#vi":6,"vi":6,"VII":7,"vii":7,"#vii":7}

l=[]
for i in range(9):
  l.append([0,0,0,0,0,0,0,0,0])

for a in dezrann2.values():
  dcmlab = pd.read_table("corelli-main/reviewed/"+a+"_reviewed.tsv")
  prec=0
  for index, row in dcmlab.iterrows():
    l[prec][dic_deg[row["numeral"]]]+=1
    prec=dic_deg[row["numeral"]]
  l[prec][8]+=1

nb = np.array(l)
print(nb)

proba=[]
for i in l:
  somme = sum(i)
  ligne=[]
  for j in i:
    if(somme!=0):
      ligne.append(round(j/somme,2))
    else:
      ligne.append(round(j,2))
  proba.append(ligne)

proba = np.array(proba)
print(proba)